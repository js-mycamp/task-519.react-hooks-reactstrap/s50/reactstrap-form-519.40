import {Row,Col,Button} from "reactstrap"
import image from "./asset/mèo nè.jpg"
function Form (){
    return(
        <div className="container">
        <Row>
            <h4>HỒ SƠ NHÂN VIÊN</h4>
        </Row>
        <Row>
            <Col xs="9">
                <Row className="form-control">
                    <Col className="mt-2">
                        <Row>
                            <Col xs="3">HỌ VÀ TÊN</Col>
                            <Col  xs="9">
                                <input className="form-control"></input>
                            </Col>
                        </Row>
                    </Col>
                    <Col className="mt-2">
                        <Row>
                            <Col  xs="3">NGÀY SINH</Col>
                            <Col  xs="9">
                            <input className="form-control"></input>
                            </Col>
                        </Row>
                    </Col>
                    <Col className="mt-2">
                        <Row>
                            <Col  xs="3">SỐ ĐIỆN THOẠI</Col>
                            <Col  xs="9">
                            <input className="form-control"></input>
                            </Col>
                        </Row>
                    </Col>
                    <Col className="mt-2">
                        <Row>
                            <Col  xs="3">GIỚI TÍNH</Col>
                            <Col  xs="9">
                            <input className="form-control"></input>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Col>
            <Col xs="3">
                <img src={image} alt='chào mừng bạn đến với devcamp' style={{ width: "290px" , height:"198px" }}/>
            </Col>
        </Row>
        <Row className="mt-2">
            <Col  xs="2"> CÔNG VIỆC </Col>
            <Col  xs="10">
                <input className="form-control"  style={{  height:"100px" }}></input>
            </Col>
        </Row>
        <Row className="mt-3">
            <Col xs="10"></Col>
            <Col xs = "2">
            <Button color="success">chi tiết</Button>{" "}  <Button color="success">kiểm tra</Button>
            </Col>
        </Row>
    </div>
    )
}
export default Form